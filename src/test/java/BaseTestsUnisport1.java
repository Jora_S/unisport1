import org.junit.Test;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.interactions.Actions;

import java.util.concurrent.TimeUnit;

import static junit.framework.TestCase.assertEquals;

/**
 * Created with IntelliJ IDEA.
 * User: georgiy
 * Date: 24.04.18
 * Time: 12:27
 * To change this template use File | Settings | File Templates.
 */
public class BaseTestsUnisport1 {
    WebDriver driver = initChromedriver();
    @Test
    public void Test1() {
        driver.get("https://unisport.ua/");
        driver.manage().timeouts().implicitlyWait(20, TimeUnit.SECONDS);
        WebElement bike = driver.findElement(By.xpath("/html/body/div[2]/div[1]/header/div[2]/div/nav/ul/li[1]/div[1]/a"));
        Actions actions = new Actions(driver);
        actions.moveToElement(bike).build().perform();
        WebElement gorniy = driver.findElement(By.cssSelector("body > div.container"));
        gorniy.click();
        WebElement bike1 = driver.findElement(By.xpath("//*[@id=\"main\"]/div/nav/div[3]/a/span"));
        bike1.click();
        WebElement begovel = driver.findElement(By.linkText("Беговел Alpina 14 Tornado Black-Green"));
        begovel.click();
        WebElement pricebegovel = driver.findElement(By.xpath("//*[@id=\"mod-block-4\"]/section/div[1]/div[1]/div[1]/div"));
        String picebegovelText = pricebegovel.getText();
        assertEquals("1 110 грн", picebegovelText);
        System.out.println("1 110 грн");
        WebElement button = driver.findElement(By.xpath("//*[@id=\"j-buy-button-widget-15534\"]/span"));
        button.click();
        WebElement model = driver.findElement(By.xpath("//*[@id=\"product_4e463ad37f77e1663f097a0e1d498242\"]/td[2]/div[1]/a"));
        String modelText = model.getText();
        assertEquals("Беговел Alpina 14 Tornado Black-Green, 2018, 12", modelText);
        System.out.println("Беговел Alpina 14 Tornado Black-Green, 2018, 12");
        WebElement pricekorzina = driver.findElement(By.xpath("//*[@id=\"product_4e463ad37f77e1663f097a0e1d498242\"]/td[2]/div[2]"));
        String pricekorzinaText = pricekorzina.getText();
        assertEquals("1 110 грн", pricekorzinaText);
        System.out.println("1 110 pokupai!!!");
        WebElement kupit = driver.findElement(By.linkText("Оформить заказ"));
        kupit.click();
        WebElement nazad = driver.findElement(By.xpath("/html/body/div[1]/header/div[1]/div/div/div[1]/div[1]/a/img"));
        nazad.click();
        WebElement turizm = driver.findElement(By.xpath("/html/body/div[2]/div[1]/header/div[2]/div/nav/ul/li[4]/div[1]/a"));
        turizm.click();

        driver.quit();
    }

    public static WebDriver initChromedriver() {
        System.setProperty("webdriver.chrome.driver", System.getProperty("user.dir") + "/drivers/chromedriver.exe");
        return new ChromeDriver() ;
    }

}
